**Circle of Fifths**

This app is designed to be a reference point for those studying music: it features an interactive circle of fifths for ear training, scales and tablature references.

**Features**

* **Major Circle of Fifths**
    A complete reference guide for the circle of fifths in the major key, their pitch and associated tablature. Click a note to hear it's pitch and view the key signature, and hold down on it to view the tablature.     
 

* **Minor Circle of Fifths**
    A complete reference guide for the circle of fifths in the minor key, their pitch and associated tablature. Click a note to hear it's pitch and view the key signature, and hold down on it to view the tablature.  


* **List of Scales**
    You can add, delete or update your own custom scales to practice or review. The app features reminders to make sure to tell you not to forget to practice and will show you your list of inputted scales.

**Installation and running the project**

Simply clone the repository and download files to view src. Running the application will place you at the Major Circle of Fifths reference point, with a navigatable menu in the top right corner.


**Author**

Manizeh Ahmed