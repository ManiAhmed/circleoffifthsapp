package com.cof.mani.circleoffifths;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.cof.mani.circleoffifths.controller.MusicNoteController;
import com.cof.mani.circleoffifths.controller.ScalesController;
import com.cof.mani.circleoffifths.model.Scale;
import com.cof.mani.circleoffifths.view.ScaleListViewActivity;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.cof.mani.circleoffifths", appContext.getPackageName());
    }

    @Test
    public void apiCallReceived() throws Exception {
        final Bitmap[] bitmapImage = new Bitmap[1];

        new AsyncTask<Void, Void, Bitmap>() {

            protected Bitmap doInBackground(Void... params) {
                try {
                    String test = ("https://ukulele-chords.com/get?ak=2585a439a28037716245c495d3967021&r=C&typ=major");
                    URL url = new URL(test);

                    XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                    factory.setNamespaceAware(true);
                    XmlPullParser xpp = factory.newPullParser();

                    HttpURLConnection connectionXML = (HttpURLConnection) url.openConnection();
                    connectionXML.setDoInput(true);
                    connectionXML.connect();
                    xpp.setInput(connectionXML.getInputStream(), null);

                    int eventType = xpp.getEventType();

                    while(eventType!= XmlPullParser.END_DOCUMENT && bitmapImage[0] == null)
                    {   Log.d("XPP CONTENTS", xpp.getText() + " ");
                        if(xpp.getText() != null && xpp.getText().startsWith("https:")) {
                            {
                                URL urlImg = new URL(xpp.getText());
                                HttpURLConnection connection=(HttpURLConnection) urlImg.openConnection();
                                connection.setDoInput(true);
                                connection.connect();
                                InputStream input = connection.getInputStream();
                                bitmapImage[0] = BitmapFactory.decodeStream(input);
                                input.close();
                                break;
                            }
                        }

                        eventType = xpp.next();
                    }
                }

                catch (XmlPullParserException e) {
                    Log.e("ERROR", "xml pull error");
                }

                catch (MalformedURLException e) {
                    Log.e("ERROR", "bad url");
                }

                catch (IOException e) {
                    Log.e("ERROR", "input stream error");
                    e.printStackTrace();
                }

                return bitmapImage[0];
            }

            protected void onPostExecute(Bitmap bm) {
                if(bm != null) {
                    assertNotEquals(null, bitmapImage);
                }
            }
        }.execute();
    }

    @Test
    public void playSound() throws Exception {
        Context appContext = InstrumentationRegistry.getTargetContext();
        MusicNoteController musicNoteController = new MusicNoteController(appContext);
        byte[] note;

        String key = "A";
        musicNoteController.open();
        note = musicNoteController.getNoteSound(key);

        MediaPlayer mediaPlayer = new MediaPlayer();
        File tempMp3 = null;
        try {
            tempMp3 = File.createTempFile(key+"Note", "mp3", appContext.getCacheDir());
            tempMp3.deleteOnExit();
            FileOutputStream fos = new FileOutputStream(tempMp3);
            fos.write(note);
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileInputStream fis = null;
        try {
            fis = new FileInputStream(tempMp3);
            mediaPlayer.reset();
            mediaPlayer.setDataSource(fis.getFD());
            mediaPlayer.prepare();
            mediaPlayer.start();

        }

        catch (FileNotFoundException e) {
            e.printStackTrace();

        }
        catch (IOException e) {
            e.printStackTrace();
        }

        assertNotEquals(null, tempMp3);
    }

    @Test
    public void startNotification() throws Exception {
        Context appContext = InstrumentationRegistry.getTargetContext();
        int importance = NotificationManager.IMPORTANCE_DEFAULT;
        NotificationChannel channel = new NotificationChannel("defaultChannel", "Default Channel", importance);
        channel.setDescription("Default");

        NotificationManager manager = (NotificationManager) appContext.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.createNotificationChannel(channel);

        Notification.Builder builder = new Notification.Builder(appContext, "defaultChannel");
        builder.setContentTitle("Notification");
        builder.setContentText("Remember to practice");
        builder.setSmallIcon(R.drawable.ic_music_note_black_24dp);
        builder.setAutoCancel(true);
        builder.setColor(appContext.getResources().getColor(R.color.colorAccent, null));

        Intent intentPending = new Intent(appContext, ScaleListViewActivity.class);
        int requestID = (int) System.currentTimeMillis();
        PendingIntent pIntent = PendingIntent.getActivity(appContext, requestID, intentPending, 0);
        builder.setContentIntent(pIntent);

        Notification notification = builder.build();
        assertNotEquals(null, notification);
    }
}
