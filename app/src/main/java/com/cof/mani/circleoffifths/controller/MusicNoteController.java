package com.cof.mani.circleoffifths.controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.cof.mani.circleoffifths.model.Scale;
import com.cof.mani.circleoffifths.view.ScaleListViewActivity;
import com.cof.mani.databaseopenhelper.DatabaseOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class MusicNoteController  {
    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase database;
    private static MusicNoteController instance;

    public MusicNoteController(Context context) {
        this.openHelper = new DatabaseOpenHelper(context);
    }

    /**
     * Gets instance.
     *
     * @param context the context
     * @return the instance
     */
    public static MusicNoteController getInstance(ScaleListViewActivity context) {
        if (instance == null) {
            instance = new MusicNoteController(context);
        }
        return instance;
    }

    /**
     * Open db.
     */
    public void open() {
        this.database = openHelper.getWritableDatabase();
    }

    /**
     * Close db.
     */
    public void close() {
        if (database != null) {
            this.database.close();
        }
    }

    /**
     * Gets staff image.
     *
     * @return the sounds in the database
     */
    public byte[] getNoteSound(String name) {
        byte[] noteStream = null;
        Cursor cursor = null;

        try {
            cursor = database.rawQuery("SELECT NoteSound FROM " + "MusicNotes" + " WHERE NoteID = ?", new String[]{name});
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                noteStream = cursor.getBlob(cursor.getColumnIndex("NoteSound"));
                break;
            }
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return noteStream;
    }
}

