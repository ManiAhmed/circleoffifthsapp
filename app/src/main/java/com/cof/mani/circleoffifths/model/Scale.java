package com.cof.mani.circleoffifths.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * The type Scale.
 */
public class Scale implements Parcelable {
    private String key;
    private String style;
    private String rhythm;

    /**
     * Instantiates a new Scale.
     */
    public Scale(){
        ;
    }

    /**
     * Instantiates a new Scale.
     *
     * @param key    the key
     * @param style  the style
     * @param rhythm the rhythm
     */
    public Scale(String key, String style, String rhythm) {
        this.key = key;
        this.style = style;
        this.rhythm = rhythm;
    }

    /**
     * Instantiates a new Scale.
     *
     * @param in the in
     */
    protected Scale(Parcel in) {
        key = in.readString();
        style = in.readString();
        rhythm = in.readString();
    }

    /**
     * The constant CREATOR.
     */
    public static final Creator<Scale> CREATOR = new Creator<Scale>() {
        @Override
        public Scale createFromParcel(Parcel in) {
            return new Scale(in);
        }

        @Override
        public Scale[] newArray(int size) {
            return new Scale[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * Gets key.
     *
     * @return the key
     */
    public String getKey() {
        return key;
    }

    /**
     * Sets key.
     *
     * @param key the key
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * Gets style.
     *
     * @return the style
     */
    public String getStyle() {
        return style;
    }

    /**
     * Sets style.
     *
     * @param style the style
     */
    public void setStyle(String style) {
        this.style = style;
    }

    /**
     * Gets rhythm.
     *
     * @return the rhythm
     */
    public String getRhythm() {
        return rhythm;
    }

    /**
     * Sets rhythm.
     *
     * @param rhythm the rhythm
     */
    public void setRhythm(String rhythm) {
        this.rhythm = rhythm;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(key);
        parcel.writeString(style);
        parcel.writeString(rhythm);

    }
}
