package com.cof.mani.circleoffifths.interfaces;

import com.cof.mani.circleoffifths.model.Scale;

/**
 * The interface Add scale fragment listener.
 */
public interface addScaleFragmentListener {
    /**
     * Add scale.
     * @param scale the scale
     */
    public void addScale(Scale scale);
}
