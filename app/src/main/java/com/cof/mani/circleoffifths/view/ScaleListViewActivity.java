package com.cof.mani.circleoffifths.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.cof.mani.circleoffifths.adapters.ScaleAdapter;
import com.cof.mani.circleoffifths.fragments.addScaleFragment;
import com.cof.mani.circleoffifths.interfaces.addScaleFragmentListener;
import com.cof.mani.circleoffifths.R;
import com.cof.mani.circleoffifths.controller.ScalesController;
import com.cof.mani.circleoffifths.model.Scale;

import java.util.List;

/**
 * The type Scale list view activity.
 */
public class ScaleListViewActivity extends AppCompatActivity implements addScaleFragmentListener {

    /**
     * The Database access.
     */
    public ScalesController databaseAccess;
    /**
     * The Lv.
     */
    RecyclerView lv;
    RecyclerView.LayoutManager layoutManager;
    ScaleAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scale_list);

        lv = (RecyclerView) findViewById(R.id.scale_list_view);
        databaseAccess = ScalesController.getInstance(this);
        databaseAccess.open();
        List<Scale> scales = databaseAccess.getScales();
        databaseAccess.close();

        adapter = new ScaleAdapter(this, scales);
        lv.setAdapter(adapter);

        layoutManager = new LinearLayoutManager(this);
        lv.setLayoutManager(layoutManager);

        registerForContextMenu(lv);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.majorKey:
                Intent moveToMajor = new Intent(this, MainActivity.class);
                startActivity(moveToMajor);
                return true;
            case R.id.minorKey:
                Intent moveToMinor = new Intent(this, MinorKeyActivity.class);
                startActivity(moveToMinor);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.list_item_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        /*//When the user clicks delete on a certain list item it will remove it from the database and refresh the activity.
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        int test = info.position;
        Scale scale = (Scale) adapter.getItemAtPosition(test);
        databaseAccess = ScalesController.getInstance(this);
        databaseAccess.open();
        databaseAccess.removeScales(scale.getKey());
        databaseAccess.close();
        finish();
        startActivity(getIntent()); */
        return true;
    }

    /**
     * On add scale event listener that shows the dialog fragment.
     *
     * @param view the view
     */
    public void onAddScale(View view) {
        addScaleFragment fragment = new addScaleFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragment.show(fragmentManager, "addScale");
    }

    /**
     * Opens database and adds input.
     *
     * @param scale the scale to be processed
     */
    public void addScale(Scale scale) {
        databaseAccess = ScalesController.getInstance(this);
        databaseAccess.open();

        if(!databaseAccess.addScale(scale)) {
            AlertDialog alert = new AlertDialog.Builder(ScaleListViewActivity.this).create();
            alert.setMessage("ERROR: Couldn't add a new scale");
            alert.setButton(android.app.AlertDialog.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            alert.show();
        }

        databaseAccess.close();
        finish();
        startActivity(getIntent());
    }
}
