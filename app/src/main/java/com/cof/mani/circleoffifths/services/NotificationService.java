package com.cof.mani.circleoffifths.services;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.provider.AlarmClock;
import android.support.annotation.Nullable;

import com.cof.mani.circleoffifths.R;
import com.cof.mani.circleoffifths.view.ScaleListViewActivity;

import java.util.Date;

/**
 * Service that runs to display a notification
 */
public class NotificationService extends Service {
    @Override
    public int onStartCommand(Intent intent, int flags, int startID) {
        super.onStartCommand(intent, flags, startID);

        int importance = NotificationManager.IMPORTANCE_DEFAULT;
        NotificationChannel channel = new NotificationChannel("defaultChannel", "Default Channel", importance);
        channel.setDescription("Default");

        NotificationManager manager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.createNotificationChannel(channel);

        Notification.Builder builder = new Notification.Builder(this, "defaultChannel");
        builder.setContentTitle("Notification");
        builder.setContentText("Remember to practice");
        builder.setSmallIcon(R.drawable.ic_music_note_black_24dp);
        builder.setAutoCancel(true);
        builder.setColor(getResources().getColor(R.color.colorAccent, null));

        Intent intentPending = new Intent(this, ScaleListViewActivity.class);
        int requestID = (int) System.currentTimeMillis();
        PendingIntent pIntent = PendingIntent.getActivity(this, requestID, intentPending, 0);
        builder.setContentIntent(pIntent);

        Notification notification = builder.build();
        manager.notify(800, notification);
        intent.setAction(String.valueOf(new Date()));
        return START_STICKY;
    }



    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
