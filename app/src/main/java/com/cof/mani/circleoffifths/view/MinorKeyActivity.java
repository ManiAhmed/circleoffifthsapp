package com.cof.mani.circleoffifths.view;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.cof.mani.circleoffifths.fragments.StaffReferenceFragment;
import com.cof.mani.circleoffifths.R;
import com.cof.mani.circleoffifths.controller.MusicNoteController;
import com.cof.mani.circleoffifths.controller.StaffReferenceController;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * The type Minor key activity.
 */
public class MinorKeyActivity extends AppCompatActivity implements View.OnClickListener, View.OnLongClickListener {
    StaffReferenceFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.minor_key_layout);

        TextView tv = findViewById(R.id.keySwitchMinor);
        registerForContextMenu(tv);

        fragment = new StaffReferenceFragment();
        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(R.id.staff_fragment_container, fragment);
        ft.commit();

        setListeners();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.majorKey:
                Intent moveToMajor = new Intent(this, MainActivity.class);
                moveToMajor.putExtra("fragID", fragment.getId());
                startActivity(moveToMajor);
                return true;
            case R.id.scaleList:
                Intent moveToScaleList = new Intent(this, ScaleListViewActivity.class);
                startActivity(moveToScaleList);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.minor_context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Intent moveToMajor = new Intent(this, MainActivity.class);
        startActivity(moveToMajor);
        return true;
    }

    /**
     * Sets listeners.
     */
    public void setListeners() {
        ImageButton ANote = (ImageButton) findViewById(R.id.note_Amin);
        ANote.setOnClickListener(this);
        ANote.setOnLongClickListener(this);
        ImageButton ENote = (ImageButton)  findViewById(R.id.note_Emin);
        ENote.setOnClickListener(this);
        ENote.setOnLongClickListener(this);
        ImageButton BNote = (ImageButton) findViewById(R.id.note_Bmin);
        BNote.setOnClickListener(this);
        BNote.setOnLongClickListener(this);
        ImageButton FsharpNote = (ImageButton) findViewById(R.id.note_FsharpMin);
        FsharpNote.setOnClickListener(this);
        FsharpNote.setOnLongClickListener(this);
        ImageButton CsharpNote = (ImageButton) findViewById(R.id.note_CsharpMin);
        CsharpNote.setOnClickListener(this);
        CsharpNote.setOnLongClickListener(this);
        ImageButton GsharpNote = (ImageButton) findViewById(R.id.note_GsharpMin);
        GsharpNote.setOnClickListener(this);
        GsharpNote.setOnLongClickListener(this);
        ImageButton EflatNote = (ImageButton) findViewById(R.id.note_EflatMin);
        EflatNote.setOnClickListener(this);
        EflatNote.setOnLongClickListener(this);
        ImageButton BflatNote = (ImageButton) findViewById(R.id.note_BflatMin);
        BflatNote.setOnClickListener(this);
        BflatNote.setOnLongClickListener(this);
        ImageButton FNote = (ImageButton) findViewById(R.id.note_Fmin);
        FNote.setOnClickListener(this);
        FNote.setOnLongClickListener(this);
        ImageButton CNote = (ImageButton) findViewById(R.id.note_Cmin);
        CNote.setOnClickListener(this);
        CNote.setOnLongClickListener(this);
        ImageButton GNote = (ImageButton) findViewById(R.id.note_Gmin);
        GNote.setOnClickListener(this);
        GNote.setOnLongClickListener(this);
        ImageButton DNote = (ImageButton) findViewById(R.id.note_Dmin);
        DNote.setOnClickListener(this);
        DNote.setOnLongClickListener(this);
    }

    /**
     * Converts bytestream to bitmap
     * @param image the image file to convert
     * @return the Bitmap format of image
     */
    public static Bitmap toBitmap(byte[] image) {
        return BitmapFactory.decodeByteArray(image, 0, image.length);
    }

    @Override
    public void onClick(View view) {







        switch (view.getId()) {
            case R.id.note_Amin:
                setFragment("G");
                playSound("G");
                break;
            case R.id.note_Emin:
                setFragment("G");
                playSound("G");
                break;
            case R.id.note_Bmin:
                setFragment("D");
                playSound("D");
                break;
            case R.id.note_FsharpMin:
                setFragment("A");
                playSound("A");
                break;
            case R.id.note_CsharpMin:
                setFragment("E");
                playSound("E");
                break;
            case R.id.note_GsharpMin:
                setFragment("B");
                playSound("B");
                break;
            case R.id.note_EflatMin:
                setFragment("GFlat");
                playSound("G");
                break;
            case R.id.note_BflatMin:
                setFragment("DFlat");
                playSound("CSharp");
            case R.id.note_Fmin:
                setFragment("AFlat");
                playSound("A");
            case R.id.note_Cmin:
                setFragment("EFlat");
                playSound("Eb");
                break;
            case R.id.note_Gmin:
                setFragment("B");
                playSound("Bb");
                break;
            case R.id.note_Dmin:
                setFragment("D");
                playSound("D");
                break;
            default:
                break;
        }
    }

    @Override
    public boolean onLongClick(View view) {
        switch (view.getId()) {
            case R.id.note_Amin:
                setChordImage("A", "minor");
                break;
            case R.id.note_Emin:
                setChordImage("E", "minor");
                break;
            case R.id.note_Bmin:
                setChordImage("B", "minor");
                break;
            case R.id.note_FsharpMin:
                setChordImage("F", "minor");
                break;
            case R.id.note_CsharpMin:
                setChordImage("C", "minor");
                break;
            case R.id.note_GsharpMin:
                setChordImage("G", "minor");
                break;
            case R.id.note_EflatMin:
                setChordImage("Eb", "minor");
                break;
            case R.id.note_BflatMin:
                setChordImage("Bb", "minor");
                break;
            case R.id.note_Fmin:
                setChordImage("F", "minor");
                break;
            case R.id.note_Cmin:
                setChordImage("C", "minor");
                break;
            case R.id.note_Gmin:
                setChordImage("G", "minor");
                break;
            case R.id.note_Dmin:
                setChordImage("D", "minor");
                break;
            default:
                break;
        }
        return true;
    }


    @SuppressLint("StaticFieldLeak")
    public void setChordImage(final String root, final String type) {
        final Bitmap[] bitmapImage = new Bitmap[1];

        new AsyncTask<Void, Void, Bitmap>() {

            protected Bitmap doInBackground(Void... params) {
                try {
                    String test = ("https://ukulele-chords.com/get?ak=2585a439a28037716245c495d3967021&r=" + root + "&typ=" + type);
                    URL url = new URL(test);

                    XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                    factory.setNamespaceAware(true);
                    XmlPullParser xpp = factory.newPullParser();

                    HttpURLConnection connectionXML = (HttpURLConnection) url.openConnection();
                    connectionXML.setDoInput(true);
                    connectionXML.connect();
                    xpp.setInput(connectionXML.getInputStream(), null);

                    int eventType = xpp.getEventType();

                    while(eventType!= XmlPullParser.END_DOCUMENT && bitmapImage[0] == null)
                    {   Log.d("XPP CONTENTS", xpp.getText() + " ");
                        if(xpp.getText() != null && xpp.getText().startsWith("https:")) {
                            {
                                URL urlImg = new URL(xpp.getText());
                                HttpURLConnection connection=(HttpURLConnection) urlImg.openConnection();
                                connection.setDoInput(true);
                                connection.connect();
                                InputStream input = connection.getInputStream();
                                bitmapImage[0] = BitmapFactory.decodeStream(input);
                                input.close();
                                break;
                            }
                        }

                        eventType = xpp.next();
                    }
                }

                catch (XmlPullParserException e) {
                    Log.e("ERROR", "xml pull error");
                }

                catch (MalformedURLException e) {
                    Log.e("ERROR", "bad url");
                }

                catch (IOException e) {
                    Log.e("ERROR", "input stream error");
                    e.printStackTrace();
                }

                return bitmapImage[0];
            }

            protected void onPostExecute(Bitmap bm) {
                if(bm != null) {
                    fragment.setImage(bm.createScaledBitmap(bm, 650, 650, false));
                }
            }
        }.execute();

    }

    public void setFragment(String key) {
        Bitmap image;
        StaffReferenceController staffReferenceController = new StaffReferenceController(this);
        staffReferenceController.open();
        byte[] data;

        data = staffReferenceController.getStaffImage(key);
        image = toBitmap(data);
        fragment.setImage(image);
        staffReferenceController.close();

    }

    public void playSound(String key) {
        MusicNoteController musicNoteController = new MusicNoteController(this);
        byte[] note;

        musicNoteController.open();
        note = musicNoteController.getNoteSound(key);

        MediaPlayer mediaPlayer = new MediaPlayer();
        File tempMp3 = null;
        try {
            tempMp3 = File.createTempFile(key+"Note", "mp3", getCacheDir());
            tempMp3.deleteOnExit();
            FileOutputStream fos = new FileOutputStream(tempMp3);
            fos.write(note);
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileInputStream fis = null;
        try {
            fis = new FileInputStream(tempMp3);
            mediaPlayer.reset();
            mediaPlayer.setDataSource(fis.getFD());
            mediaPlayer.prepare();
            mediaPlayer.start();

        }

        catch (FileNotFoundException e) {
            e.printStackTrace();

        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
