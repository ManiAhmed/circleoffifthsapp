package com.cof.mani.circleoffifths.fragments;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.cof.mani.circleoffifths.R;

/**
 * Created by Mani on 4/8/2018.
 */
public class StaffReferenceFragment extends Fragment {
    ImageView image;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.staff_reference_layout, parent, false);
        image = (ImageView) root.findViewById(R.id.staffReference);
        return root;
    }

    /**
     * Sets image.
     *
     * @param bm the Bitmap of image to set
     */
    public void setImage(Bitmap bm) {
        image.setImageBitmap(bm);
    }
}
