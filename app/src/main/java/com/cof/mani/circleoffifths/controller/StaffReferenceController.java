package com.cof.mani.circleoffifths.controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.cof.mani.circleoffifths.model.Scale;
import com.cof.mani.circleoffifths.view.ScaleListViewActivity;
import com.cof.mani.databaseopenhelper.DatabaseOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class StaffReferenceController {
    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase database;
    private static StaffReferenceController instance;

    public StaffReferenceController(Context context) {
        this.openHelper = new DatabaseOpenHelper(context);
    }

    /**
     * Gets instance.
     *
     * @param context the context
     * @return the instance
     */
    public static StaffReferenceController getInstance(ScaleListViewActivity context) {
        if (instance == null) {
            instance = new StaffReferenceController(context);
        }
        return instance;
    }

    /**
     * Open db.
     */
    public void open() {
        this.database = openHelper.getWritableDatabase();
    }

    /**
     * Close db.
     */
    public void close() {
        if (database != null) {
            this.database.close();
        }
    }

    /**
     * Gets staff image.
     *
     * @return the scales in the database
     */
    public byte[] getStaffImage(String name) {
        byte[] imageStream = null;
        Cursor cursor = null;

        try {
            cursor = database.rawQuery("SELECT Image FROM " + "StaffImgs" + " WHERE ImageID = ?", new String[]{name});
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                imageStream = cursor.getBlob(cursor.getColumnIndex("Image"));
                break;
            }
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return imageStream;
    }
}
