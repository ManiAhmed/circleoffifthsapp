package com.cof.mani.circleoffifths.adapters;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cof.mani.circleoffifths.R;
import com.cof.mani.circleoffifths.model.Scale;

import java.util.List;

/**
 * The type Scale adapter.
 */
public class ScaleAdapter extends RecyclerView.Adapter<ScaleAdapter.ViewHolder>{
    private List<Scale> scales;
    private Context context;
    private LayoutInflater inflater;



    /**
     * Instantiates a new Scale adapter.
     * @param scales  the scales
     */
    public ScaleAdapter(Context context, List<Scale> scales) {
        inflater = LayoutInflater.from(context);
        this.scales = scales;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View scaleView = inflater.inflate(R.layout.activity_scale_list_view, parent, false);

            ViewHolder viewHolder = new ViewHolder(scaleView);
            return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final Scale scale = scales.get(position);

        holder.txtKey.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View view) {
                scales.remove(scales.get(position));
                notifyDataSetChanged();
                Snackbar.make(view, "Deleted " + scale.getKey(), Snackbar.LENGTH_LONG).show();
                return true;
            }
        });
        TextView txtKeyView = holder.txtKey;
        txtKeyView.setText(scale.getKey() + "");

        holder.txtStyle.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View view) {
                scales.remove(scales.get(position));
                notifyDataSetChanged();
                Snackbar.make(view, "Deleted " + scale.getKey(), Snackbar.LENGTH_LONG).show();
                return true;
            }
        });
        TextView txtStyleView = holder.txtStyle;
        txtStyleView.setText(scale.getStyle() + "");

        holder.txtRhythm.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View view) {
                scales.remove(scales.get(position));
                notifyDataSetChanged();
                Snackbar.make(view, "Deleted " + scale.getKey(), Snackbar.LENGTH_LONG).show();
                return true;
            }
        });
        TextView txtRhythmView = holder.txtRhythm;
        txtRhythmView.setText(scale.getRhythm() + "");
    }

    @Override
    public int getItemCount() {
        return scales.size();

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtKey;
        public TextView txtStyle;
        public TextView txtRhythm;

        public ViewHolder(View itemView) {
            super(itemView);
            txtKey = (TextView) itemView.findViewById(R.id.txtKey);
            txtStyle = (TextView) itemView.findViewById(R.id.txtStyle);
            txtRhythm = (TextView) itemView.findViewById(R.id.txtRhythm);
        }
    }

}


