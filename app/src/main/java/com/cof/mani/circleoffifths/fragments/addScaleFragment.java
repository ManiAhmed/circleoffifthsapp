package com.cof.mani.circleoffifths.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.cof.mani.circleoffifths.interfaces.addScaleFragmentListener;
import com.cof.mani.circleoffifths.R;
import com.cof.mani.circleoffifths.model.Scale;

/**
 * The type Add scale fragment.
 */
public class addScaleFragment extends DialogFragment {

    View view;
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final addScaleFragmentListener listener = (addScaleFragmentListener) getActivity();
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = getActivity().getLayoutInflater();
            view = inflater.inflate(R.layout.dialog_box_layout, null);
            builder.setView(view);

            builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {

                    EditText key = (EditText) view.findViewById(R.id.editTextKey);
                    String keyString = (key.getText().toString());

                    if(!(keyString.toCharArray()[0] >= 65 && keyString.toCharArray()[0] <= 71)){
                        android.support.v7.app.AlertDialog alert = new android.support.v7.app.AlertDialog.Builder(getActivity()).create();
                        alert.setMessage("ERROR: Please enter a value from A-G");
                        alert.setButton(android.app.AlertDialog.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                        alert.show();
                    }

                    EditText style = (EditText) view.findViewById(R.id.editTextStyle);
                    String styleString = (style.getText().toString());

                    EditText rhythm = (EditText) view.findViewById(R.id.editTextRhythm);
                    String rhythmString = (rhythm.getText().toString());

                    Scale scale = new Scale(keyString, styleString, rhythmString);
                    listener.addScale(scale);

                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    addScaleFragment.this.getDialog().cancel();
                }
            });

            return builder.create();
        }
    }
