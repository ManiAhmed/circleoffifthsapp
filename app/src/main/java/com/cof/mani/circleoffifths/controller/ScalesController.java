package com.cof.mani.circleoffifths.controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.cof.mani.circleoffifths.model.Scale;
import com.cof.mani.circleoffifths.view.ScaleListViewActivity;
import com.cof.mani.databaseopenhelper.DatabaseOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Database access helper.
 */
public class ScalesController {
    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase database;
    private static ScalesController instance;

    private ScalesController(Context context) {
        this.openHelper = new DatabaseOpenHelper(context);
    }

    /**
     * Gets instance.
     *
     * @param context the context
     * @return the instance
     */
    public static ScalesController getInstance(ScaleListViewActivity context) {
        if (instance == null) {
            instance = new ScalesController(context);
        }
        return instance;
    }

    /**
     * Open db.
     */
    public void open() {
        this.database = openHelper.getWritableDatabase();
    }

    /**
     * Close db.
     */
    public void close() {
        if (database != null) {
            this.database.close();
        }
    }

    /**
     * Gets scales.
     *
     * @return the scales in the database
     */
    public List<Scale> getScales() {
        List<Scale> scales = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT * FROM scales", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Scale scale = new Scale();
            scale.setKey(cursor.getString(0));
            scale.setStyle(cursor.getString(1));
            scale.setRhythm(cursor.getString(2));
            scales.add(scale);
            cursor.moveToNext();
        }
        cursor.close();
        return scales;
    }

    /**
     * Remove scales.
     *
     * @param key the key (unique id)
     */
    public void removeScales(String key) {
        database.delete("scales", "key = ?", new String[]{key});
    }

    /**
     * Add scale, returns true if successful.
     *
     * @param scale the scale to be added
     * @return true if scale was added
     */
    public boolean addScale(Scale scale) {
        database.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            values.put("key", scale.getKey());
            values.put("style", scale.getStyle());
            values.put("rhythm", scale.getRhythm());
            database.insert("scales", null, values);
            database.setTransactionSuccessful();
            return true;
        }

        catch(Exception e) {
            Log.e("Database error", "Database insert error");
            return false;
        }

        finally {
            database.endTransaction();
        }
    }
}

